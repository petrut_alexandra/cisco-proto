//
//  AuthViewController.swift
//  CiscoApp
//
//  Created by petrut alexandra on 24/02/2019.
//  Copyright © 2019 petrut alexandra. All rights reserved.
//

import UIKit


class AuthViewController: UIViewController {
    
    var username: String?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var textFieldContainer: UIView!
    var nameTextField: WebXCustomTextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (WebxAuthHelper.authentificatior.authorized) {
            self.showInitCallVC()
            return
        }
        self.customiseTextField()
    }
    
    func customiseTextField() {
        self.nameTextField = WebXCustomTextField.init(frame: self.textFieldContainer.bounds)
        self.nameTextField?.backgroundColor = UIColor.white
        self.nameTextField?.placeholder = "Max Mustermann"
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
        nameTextField!.leftView = paddingView
        nameTextField!.leftViewMode = .always
        
        textFieldContainer.backgroundColor = UIColor.clear
        textFieldContainer.addSubview( self.nameTextField!)
        
    }
    
    func authUser(name: String) {
        self.activityIndicator.alpha = 1
        self.activityIndicator.startAnimating()
        let webxAuth = WebxAuthHelper.init(username: name)
        webxAuth.authUser(completionHandler: {
            [weak self] succes, error in
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.alpha = 0
            if (succes) {
                self?.showInitCallVC()
            } else {
                self?.showAuthFailedError(error: error)
            }
        })
    }
    
    func showAuthFailedError(error: Error?) {
        var message = "Authentification failed, try again!"
        if let err = error {
            message = err.localizedDescription
        }
        self.present(self.simpleAlert(title: "Error!", message: message), animated: true, completion: nil)
    }
    
    func showInitCallVC() {
        self.performSegue(withIdentifier: "goToInitACall", sender: nil)
    }

    
}

//MARK:- Ui Handlers

extension AuthViewController: UITextFieldDelegate {
    
    @IBAction func goButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        guard let usernameString = nameTextField?.text else {
            self.present(self.simpleAlert(title: "Error!", message: "Please input a name!"), animated: true, completion: nil)
            return
        }
        self.authUser(name: usernameString)
    }
    
    
    @IBAction func screenTapped(_ sender: Any) {
        self.view.endEditing(true)
    }
}

