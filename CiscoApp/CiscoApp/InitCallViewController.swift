//
//  InitCallViewController.swift
//  CiscoApp
//
//  Created by petrut alexandra on 24/02/2019.
//  Copyright © 2019 petrut alexandra. All rights reserved.
//

import UIKit

class InitCallViewController: UIViewController {
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startButton.layer.cornerRadius = 0.5 * self.startButton.frame.size.width
    }
    
    @IBAction func callButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "showCallVc", sender: nil)
    }
}
