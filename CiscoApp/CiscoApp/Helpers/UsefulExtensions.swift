//
//  UsefulExtensions.swift
//  CiscoApp
//
//  Created by petrut alexandra on 24/02/2019.
//  Copyright © 2019 petrut alexandra. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func decodedBase64Data() -> Data {
        return Data(base64Encoded: self)!
    }
    
}

extension UIViewController {
    
    func simpleAlert(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        return alert
    }
}
