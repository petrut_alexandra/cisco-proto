//
//  WebxAuthHelper.swift
//  CiscoApp
//
//  Created by petrut alexandra on 24/02/2019.
//  Copyright © 2019 petrut alexandra. All rights reserved.
//

import Foundation
import WebexSDK
import SwiftJWT

let secretToken = "lF8JfFxklkffLcXsrNjS35nYm1p23SwhKMigEGneU1Y="
let guestToken = "Y2lzY29zcGFyazovL3VzL09SR0FOSVpBVElPTi9mZTJhZjI1MS02ZjNiLTQ5NmQtODU0ZS0wZjFkYTBkYWU3NTE"

struct WebXClaims: Claims {
    var name: String
    var iss: String
    var exp: String
    var sub: String
}

class WebxAuthHelper: NSObject {
    
    var username: String?
    static var authentificatior = JWTAuthenticator()
    
    init(username: String) {
        self.username = username
    }
    
    private func webXJWT() -> JWT<WebXClaims> {
        let header = Header(typ: "JWT")
        let claims = WebXClaims(name: self.username!, iss: guestToken, exp: "1611486849", sub: UIDevice.current.identifierForVendor!.uuidString)
        return JWT(header: header, claims: claims)
    }
    
    private func jwtString() -> String? {
        let jwtSigner = JWTSigner.hs256(key: secretToken.decodedBase64Data())
        
        var jwt = self.webXJWT()
        return try? jwt.sign(using: jwtSigner)
    }
    
    private func verityJwt() -> Bool {
        let jwtVerifier = JWTVerifier.hs256(key: secretToken.decodedBase64Data())
        return JWT<WebXClaims>.verify(self.jwtString()!, using: jwtVerifier)
    }
    
    func authUser(completionHandler: @escaping (Bool, Error?) -> ()) {
        
        guard let jwtString = self.jwtString() else {
            completionHandler(false, nil)
            return
        }
        
        if (!self.verityJwt()) {
            completionHandler(false, nil)
            return
        }
        
        let authenticator = JWTAuthenticator()
        if !authenticator.authorized {
            authenticator.authorizedWith(jwt: jwtString)
        }
        
        WebxAuthHelper.authentificatior = authenticator
        
        if (!Webex(authenticator: authenticator).phone.registered) {
            Webex(authenticator: authenticator).phone.register() { error in
                if let _ = error {
                    completionHandler(false, error)
                } else {
                    completionHandler(true, nil)
                }
            }
        }
        
    }
    
}
