//
//  WebXCustomTextField.swift
//  CiscoApp
//
//  Created by Alexandra Petrut on 25/02/2019.
//  Copyright © 2019 petrut alexandra. All rights reserved.
//

import UIKit

let activeColor = UIColor(red: 20/255, green: 102/255, blue: 178/255, alpha: 1)
let inactiveColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1)

class WebXCustomTextField: UITextField, UITextFieldDelegate {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialCustomisation()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialCustomisation()
    }
    
    
    func initialCustomisation() {
        self.delegate = self
        self.textColor = activeColor
        self.font = UIFont(name: "TitilliumWeb-Light", size: 15)
        self.backgroundColor = UIColor.white
        self.clearButtonMode = .whileEditing
        self.clipsToBounds = true
        self.addDefaultBorderColor()
    }
    
    func addDefaultBorderColor() {
        self.layer.borderColor = inactiveColor.cgColor
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 2
    }
    
    func addEditingBorderColor() {
        self.layer.borderColor = activeColor.cgColor
        self.textColor = activeColor
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.addEditingBorderColor()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.addDefaultBorderColor()
        self.textColor = UIColor.black
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return false
    }
    
}
