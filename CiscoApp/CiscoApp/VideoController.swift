//
//  VideoController.swift
//  CiscoApp
//
//  Created by petrut alexandra on 24/02/2019.
//  Copyright © 2019 petrut alexandra. All rights reserved.
//

import UIKit
import WebexSDK

class VideoController: UIViewController {
    @IBOutlet weak var selfView: MediaRenderView!
    @IBOutlet weak var otherView: MediaRenderView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var endButton: UIButton!
    
    /// saparkSDK reperesent for the WebexSDK API instance
    var webexSDK: Webex?
    
    /// currentCall represent current processing call instance
    var currentCall: Call?
    
    private var callStatus:CallStatus = .initiated
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.endButton.layer.cornerRadius = 0.5 * self.endButton.frame.size.width
        self.webexSDK = Webex(authenticator: WebxAuthHelper.authentificatior)
        NotificationCenter.default.addObserver(self, selector: #selector(hangup), name: NSNotification.Name(rawValue: ApplicationShouldEndCallNotification), object: nil)
        self.createTheCall()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        self.currentCall = nil

    }
    
    func setupProcessCallbacks() {
        self.otherView.bringSubviewToFront(self.endButton)
        /* Callback when remote participant(s) answered and this *call* is connected. */
        self.currentCall!.onConnected = { [weak self] in
            if let strongSelf = self {
                strongSelf.callStatus = .connected
                let alert = self?.simpleAlert(title: "Success", message: "Call connected")
                self?.present(alert!, animated: true, completion: nil)
            }
        }
        
        self.currentCall!.onDisconnected = {  [weak self] reason in
            self?.callStatus = .disconnected
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: { _ in
                self?.navigationController?.popViewController(animated: true)
            })
            let alert = UIAlertController(title: "", message: "Call disconnecred, reason: \(reason)", preferredStyle: .alert)
            alert.addAction(okAction)
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func setupEverythingOn() {
        self.backgroundImageView.isHidden = true
        self.backgroundImageView.alpha = 0
        self.webexSDK?.phone.defaultLoudSpeaker = true
        self.webexSDK?.phone.defaultFacingMode = .user
        self.currentCall?.sendingVideo = true
        self.currentCall?.sendingAudio = true
        self.currentCall?.receivingVideo = true
        self.currentCall?.receivingAudio = true
    }
    
    @IBAction func endButtonTapped(_ sender: Any) {
        self.hangup()
    }
}

//MARK:- Call handlings
extension VideoController {
    
    func registerDeviceIfNeeded() {
        let authenticator = WebxAuthHelper.authentificatior
        if (!Webex(authenticator: authenticator).phone.registered) {
            Webex(authenticator: authenticator).phone.register() { error in
                let alert = self.simpleAlert(title: "Error", message: "Device could not be registered!")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func hangup() {
        if let currentCall = self.currentCall {
            currentCall.hangup(completionHandler: {
                error in
                if error != nil {
                    let alert = self.simpleAlert(title: "Success", message: "Call ended successfully")
                    self.present(alert, animated: true, completion: nil)
                }
            })
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    
    }
    
    func createTheCall() {
        self.otherView.bringSubviewToFront(self.endButton)

        self.webexSDK?.phone.register() { [weak self] error in
            guard let err = error else {
                self?.call()
                return
            }
            let alert = self?.simpleAlert(title: "Error", message: "\(err.localizedDescription)")
            self?.present(alert!, animated: true, completion: nil)
        }
       
    }
    
    func call() {
        self.activityIndicator.alpha = 1
        self.activityIndicator.startAnimating()
        let mediaOption = MediaOption.audioVideo(local: self.selfView, remote: self.otherView)
        
        /* Makes a call to an intended recipient on behalf of the authenticated user.*/
        self.callStatus = .initiated
        
        self.webexSDK?.phone.dial(defaultCallAddress, option: mediaOption) { [weak self] result in
            if let strongSelf = self {
                switch result {
                case .success(let call):
                    strongSelf.currentCall = call
                    self?.setupProcessCallbacks()
                    self?.setupEverythingOn()
                    strongSelf.activityIndicator.stopAnimating()
                    strongSelf.activityIndicator.alpha = 0
                    print("success")
                case .failure(let error):
                    let alert = self?.simpleAlert(title: "Error", message: "Dial call error: \(error)")
                    self?.present(alert!, animated: true, completion: nil)
                }
                
            }
        }
    }
    
}


